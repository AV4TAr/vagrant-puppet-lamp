
exec { 'apt-get update':
  command => 'apt-get update',
  path    => '/usr/bin/',
  timeout => 60,
  tries   => 3,
}

class { 'apt':
  always_apt_update => true,
}

package { ['python-software-properties']:
  ensure  => 'installed',
  require => Exec['apt-get update'],
}

file { '/home/vagrant/.bash_aliases':
  source => 'puppet:///modules/puphpet/dot/.bash_aliases',
  ensure => 'present',
}

package { ['build-essential', 'vim', 'curl', 'tmux']:
  ensure  => 'installed',
  require => Exec['apt-get update'],
}

class { 'apache': }

apache::dotconf { 'custom':
  content => 'EnableSendfile Off',
}

apache::module { 'rewrite': }
apache::module { 'ssl': }

apache::vhost { 'default':
    docroot             => '/srv/netbeans',
    server_name         => false,
    priority            => '',
    template            => 'apache/virtualhost/vhost.conf.erb',
}

apache::vhost { 'ba3.vbox8':
  server_name   => 'ba3.vbox8',
  serveraliases => [],
  docroot       => '/srv/netbeans/case-ba3/zf2src/public',
  port          => '80',
  env_variables => ['APPLICATION_ENV development'],
  priority      => '1',
}

apache::vhost { 'ba3mvp.vbox8':
  server_name   => 'ba3mvp.vbox8',
  serveraliases => [],
  docroot       => '/srv/netbeans/case-ba3-mvp/zf2src/public',
  port          => '80',
  env_variables => ['APPLICATION_ENV development'],
  priority      => '100',
}

apt::ppa { 'ppa:ondrej/php5':
  before  => Class['php'],
}

class { 'php':
  service => 'apache',
  require => Package['apache'],
}

php::module { 'php5-cli': }
php::module { 'php5-curl': }
php::module { 'php5-gd': }
php::module { 'php5-intl': }
php::module { 'php5-mcrypt': }
php::module { 'php5-mysql': }
php::module { 'php5-pgsql': }
php::module { 'php-apc': }
php::module { 'phpmyadmin': }

class { 'php::devel':
  require => Class['php'],
}

class { 'php::pear':
  require => Class['php'],
}


php::pecl::module { 'memcached':
  use_package => false,
}

php::pecl::module { 'mongo':
  use_package => false,
}


class { 'xdebug':
  service => 'apache',
}

xdebug::config { 'cgi': }
xdebug::config { 'cli': }

class { 'php::composer': }

php::ini { 'php':
  value  => ['date.timezone = "America/Montevideo"'],
  target => 'php.ini',
  service => 'apache',
}
php::ini { 'custom':
  value  => ['display_errors = On', 'error_reporting = -1'],
  target => 'custom.ini',
  service => 'apache',
}

class { 'mysql':
  root_password => 'root',
}

mysql::grant { 'diego':
  mysql_privileges     => 'ALL',
  mysql_db             => 'diego',
  mysql_user           => 'diego',
  mysql_password       => 'diego',
  mysql_host           => 'localhost',
  mysql_grant_filepath => '/home/vagrant/puppet-mysql',
}

mysql::grant { 'caseba3':
  mysql_privileges     => 'ALL',
  mysql_db             => 'case-ba3',
  mysql_user           => 'dev',
  mysql_password       => 'dev',
  mysql_host           => 'localhost',
  mysql_grant_filepath => '/home/vagrant/puppet-mysql',
}

class {'mongodb':
  enable_10gen => true,
}

#Agrego el phphmyadmin como alias en apache
exec { "phpmyadmin apache conf":
  command => "cat /etc/phpmyadmin/apache.conf > /etc/apache2/sites-enabled/phpmyadmin",
  path => "/usr/local/bin/:/bin/",
}

#Archivo apc
exec { "apc conf":
  command => "cp /usr/share/doc/php-apc/apc.php /srv/netbeans/",
  path => "/usr/local/bin/:/bin/",
}


#file { 'phpmyadmin.conf':
#    ensure  => link,
#    path    => '/etc/apache2/sites-enabled/50-phpmyadmin.conf',
#    target  => '/etc/phpmyadmin/apache.conf',
#    require => Class['phpmyadmin'],
#    notify  => Service['apache2'],
#}

#Agrego el phphmyadmin como alias en apache
exec { "mongo ini":
  command => "echo 'extension=mongo.so' > /etc/php5/conf.d/mongo.ini",
  path => "/usr/local/bin/:/bin/",
}

